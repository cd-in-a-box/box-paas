package main

import (
	"fmt"
	"log"
	"net/http"
    "time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type App struct {}

func (app App) run(mux *http.ServeMux) {
	mux.HandleFunc("/", app.trace(app.hello))
	mux.HandleFunc("/ping", app.log(app.ping))

	mux.HandleFunc("/metrics", app.log(app.handler(promhttp.Handler())))
}

func (app App) ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ping? pong!"))
}

func (app App) hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Ciao! Hai richiesto: %s", r.URL.Path)
}

func (app App) handler(h http.Handler) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        h.ServeHTTP(w, r)
    }
}

func (app App) log(next http.HandlerFunc) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
	    log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL.Path)
        next(w, r)
    }
}

func (app App) trace(next http.HandlerFunc) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        start := time.Now()
        defer func() {
            end := time.Now()
	        log.Printf("%s %s %s [%d]", r.RemoteAddr, r.Method, r.URL.Path, end.Sub(start))
        }()
        next(w, r)
    }
}

func main() {
	app := &App{}
	app.run(http.DefaultServeMux)

    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatal(err)
    }
}
