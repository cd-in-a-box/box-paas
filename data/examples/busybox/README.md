#### Build

`docker build --rm -t registry.ws.so/busybox .`

#### Run

`docker run -ti registry.ws.so/busybox`

#### Push

`docker push registry.ws.so/busybox`
