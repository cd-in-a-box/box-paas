job "fabio" {
  datacenters = ["dc1"]
  type        = "system"

  update {
    stagger      = "10s"
    max_parallel = 1
  }

  group "fabio" {
    count = 1

    restart {
      interval = "5m"
      attempts = 10
      delay    = "25s"
      mode     = "delay"
    }

    task "fabio" {
      driver = "docker"

      config {
        image = "fabiolb/fabio:1.5.11-go1.11.5"

        network_mode = "host"

        volumes = [
          "custom/fabio.properties:/etc/fabio/fabio.properties",
        ]
      }

      template {
        # source = "fabio.properties"
        destination = "custom/fabio.properties"

        data = <<EOF
proxy.addr = :9999,:3022;proto=tcp
EOF
      }

      service {
        name = "fabio"
        tags = ["global", "fabio", "ui", "lb", "http", "urlprefix-fabio.ws.so/"]
        port = "ui"

        check {
          type         = "http"
          path         = "/health"
          interval     = "10s"
          timeout      = "2s"
          address_mode = "host"
        }
      }

      resources {
        cpu    = 200
        memory = 128

        network {
          port "lb" {
            static = 9999
          }

          port "ui" {
            static = 9998
          }
        }
      }
    }
  }
}
