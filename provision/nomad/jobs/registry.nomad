job "registry" {
  datacenters = ["dc1"]
  type        = "service"

  update {
    stagger      = "10s"
    max_parallel = 1
  }

  group "registry" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "registry" {
      driver = "docker"

      config {
        image = "registry:2"

        port_map {
          http = 5000
        }

        volumes = [
          "/vagrant/data/registry:/var/lib/registry",
        ]
      }

      resources {
        cpu    = 300
        memory = 256

        network {
          port "http" {}
        }
      }

      service {
        name = "registry"
        tags = ["global", "registry", "http", "urlprefix-registry.ws.so/"]
        port = "http"

        check {
          type     = "http"
          path     = "/v2/"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
