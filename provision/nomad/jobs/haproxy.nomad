job "haproxy" {
  datacenters = ["dc1"]
  type        = "service"

  update {
    stagger      = "10s"
    max_parallel = 1
  }

  group "haproxy" {
    count = 1

    restart {
      interval = "5m"
      attempts = 10
      delay    = "25s"
      mode     = "delay"
    }

    task "haproxy" {
      driver = "docker"

      config {
        image = "haproxy"

        network_mode = "host"

        port_map {
          http = 80
        }

        volumes = [
          "custom/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg",
        ]
      }

      template {
        # source = "haproxy.cfg.tpl"
        destination = "custom/haproxy.cfg"

        data = <<EOF
global
  # debug
  maxconn 4096

defaults
  log global
  option httplog
  option dontlognull
  option forwardfor
  option http-server-close
  stats enable
  stats uri /haproxy-stats
  timeout connect 5000
  timeout client 50000
  timeout server 50000

frontend http
  bind *:80
  acl nomad hdr(host) -i nomad.ws.so
  acl consul hdr(host) -i consul.ws.so
  acl jenkins hdr(host) -i jenkins.ws.so
  use_backend _nomad if nomad
  use_backend _consul if consul
  use_backend _jenkins if jenkins
  default_backend _fabio

backend _nomad
  mode http
  server registry nomad:4646

backend _consul
  mode http
  server registry consul:8500

backend _jenkins
  mode http
  server registry jenkins:8080

backend _fabio
  mode http
  server fabio fabio:9999
EOF
      }

      service {
        name = "haproxy"
        tags = ["global", "lb", "http", "tcp", "urlprefix-nomad.ws.so/", "urlprefix-consul.ws.so/", "urlprefix-jenkins.ws.so/", "urlprefix-/"]
        port = "http"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }

      resources {
        cpu    = 300
        memory = 128

        network {
          port "http" {
            static = 80
          }
        }
      }
    }
  }
}
