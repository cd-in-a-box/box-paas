job "jenkins" {
  datacenters = ["dc1"]
  type        = "service"

  update {
    stagger      = "10s"
    max_parallel = 1
  }

  group "jenkins" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "fail"
    }

    task "jenkins" {
      driver = "docker"

      env {
        JENKINS_USERNAME               = "jenkins"
        JENKINS_PASSWORD               = "jenkins"
        DISABLE_JENKINS_INITIALIZATION = true
      }

      config {
        image = "bitnami/jenkins:2"

        network_mode = "host"

        port_map {
          http  = 8080
          https = 8443
          slave = 50000
        }

        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock",
          "/var/lib/jenkins:/bitnami/jenkins/jenkins_home",
        ]
      }

      resources {
        cpu    = 500
        memory = 512

        network {
          port "http"{}
          port "https"{}
          port "slave"{}
        }
      }

      service {
        name = "jenkins"
        tags = ["global", "jenkins", "http"]
        # tags = ["global", "jenkins", "http", "urlprefix-jenkins.ws.so/"]
        port = "http"

        check {
          type = "tcp"
          # type         = "http"
          # path         = "/"
          interval = "10s"
          timeout = "2s"
          address_mode = "driver"
        }
      }
    }
  }
}
