#!/bin/bash -e

NOMAD_VERSION=0.9.1
CONSUL_VERSION=1.5.1

echo "Installing Nomad..."
(
  cd /tmp/
  curl -sSL https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -o nomad.zip
  unzip -o nomad.zip
  install nomad /usr/bin/nomad
  rm nomad.zip
)
mkdir -p /etc/nomad.d
cp -a /vagrant/provision/nomad/*.hcl /etc/nomad.d/
chmod 755 /etc/nomad.d
cp -v /vagrant/provision/nomad/systemd/nomad.service /lib/systemd/system/
systemctl enable nomad.service
systemctl restart nomad.service
nomad version
sudo -u vagrant nomad -autocomplete-install || true

echo "Installing Consul..."
(
  cd /tmp/
  curl -qfsSL https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip -o consul.zip
  unzip -o consul.zip
  install consul /usr/bin/consul
  rm consul.zip
)
cp -v /vagrant/provision/nomad/systemd/consul.service /lib/systemd/system/
systemctl enable consul.service
systemctl restart consul.service

for bin in cfssl cfssl-certinfo cfssljson
do
  echo "Installing $bin..."
  curl -qfsSL https://pkg.cfssl.org/R1.2/${bin}_linux-amd64 > /tmp/${bin}
  install /tmp/${bin} /usr/local/bin/${bin}
done
