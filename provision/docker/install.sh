#!/bin/bash -e

DOCKER_COMPOSE_VERSION=1.24.0

# echo "Installing Certificates..."
# mkdir -p /etc/docker/certs.d/registry.ws.so || true
# cp -va /vagrant/provision/docker/certs/registry.crt /etc/docker/certs.d/registry.ws.so/ca.crt

echo "Installing Docker..."
# apt-get update -y -q
apt-get remove -y -f docker docker-engine docker.io
apt-get install -y -q apt-transport-https ca-certificates curl software-properties-common -y
curl -qfsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y -q
apt-get install -y -q docker-ce

# Enable remote API
mkdir -p /etc/systemd/system/docker.service.d || true
cp -v /vagrant/provision/docker/00-startup-options.conf /etc/systemd/system/docker.service.d/00-startup-options.conf
cp -v /vagrant/provision/docker/daemon.json /etc/docker/daemon.json
systemctl daemon-reload

# Restart docker to make sure we get the latest version of the daemon if there is an upgrade
systemctl restart docker.service

# Make sure we can actually use docker as the vagrant user
usermod -aG docker vagrant
usermod -aG docker ubuntu
docker --version

echo "Installing Docker Compose..."
curl -qfsSL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version